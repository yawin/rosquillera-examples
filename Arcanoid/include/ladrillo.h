#ifndef LADRILLO_H
#define LADRILLO_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class Ladrillo : public RF_Process
{
  public:
    Ladrillo():RF_Process("Ladrillo"){}
    virtual ~Ladrillo(){}

    virtual void Update(){}
};

#endif //LADRILLO_H
