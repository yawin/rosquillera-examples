#ifndef PELOTA_H
#define PELOTA_H

#include <RosquilleraReforged/rf_process.h>
using namespace RF_Structs;

class Pelota : public RF_Process
{
  public:
    Pelota():RF_Process("Pelota"){}
    virtual ~Pelota(){}

    virtual void Start();
    virtual void Update();
    bool state = true;

  private:
    Vector2<float> speed;
    float max_spd = 500.0;
};

#endif //PELOTA_H
