#include "../include/pelota.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_collision.h>
#include <RosquilleraReforged/rf_soundmanager.h>
#include <cstdlib>
using namespace std;
using namespace RF_Collision;

#include "../include/mainprocess.h"

void Pelota::Start()
{
  RF_AssetManager::loadAssetPackage("./res/common");
  graph = RF_AssetManager::Get<RF_Gfx2D>("common", "pelota");

  transform.position.x = 320;
  transform.position.y = 360;

  speed.x = 200.0 - (float)(rand()%400);
  speed.y = 100.0 + (float)(rand()%200);
}

void Pelota::Update()
{
  if(!MainProcess::gameStarted){return;}

  //Si choca contra la plancha
    RF_Process *collided = checkCollisionByType(this, "Plancha");
    if(collided != nullptr && speed.y > 0.0)
    {
      speed.y = -speed.y;

      float modifier = abs((int)(collided->transform.position.x - transform.position.x))/(collided->graph->w*0.5);
      if(speed.x < 0.0){ modifier *= -1.0;}

      speed.x = max_spd * modifier;
    }

  //Si choca con un ladrillo
    collided = checkCollisionByType(this, "Ladrillo");
    if(collided != nullptr && collided->signal != S_KILL)
    {
      speed.y = -speed.y;
      RF_SoundManager::playFX("ladrillos", "ladrilloSFx");
      RF_Engine::sendSignal(collided, S_KILL);
    }


  //Si choca contra los bordes de la pantalla
    if(transform.position.y < 6.0 && speed.y < 0.0)
    {
      speed.y = -speed.y;
    }
    if( (transform.position.x < 6.0 && speed.x < 0.0)
        || (transform.position.x > 634.0 && speed.x > 0.0) )
    {
      speed.x = -speed.x;
    }

  //Si la pelota muere
  if(transform.position.y > 648.0)
  {
    state = false;
  }

  transform.position.x += speed.x * RF_Engine::instance->Clock.deltaTime;
  transform.position.y += speed.y * RF_Engine::instance->Clock.deltaTime;
}
