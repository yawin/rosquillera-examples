#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <RosquilleraReforged/rf_assetmanager.h>

#include "../include/plancha.h"
#include "../include/pelota.h"
#include "../include/ladrillo.h"

void MainProcess::Start()
{
    RF_Engine::addWindow("Arcanoid", 640, 480);
    prepareGame();
}

void MainProcess::Update()
{
  if(RF_Input::key[_esc])
  {
    RF_Engine::Status()=false;
  }

  if(RF_Input::key[_return] && !gameStarted)
  {
    gameStarted = true;
  }

  if(!RF_Engine::getTask<Pelota>(pelota)->state)
  {
    preparePlayer();
  }

  if(RF_TaskManager::instance->getTaskByType("Ladrillo").size() == 0)
  {
    prepareGame();
  }
}

//Rojo Morado Azul Verde Amarillo
void MainProcess::prepareGame()
{
  RF_AssetManager::loadAssetPackage("./res/ladrillos");
  string ladrillo = "";
  string colores[5] = {"rojo", "morado", "azul", "verde", "amarillo"};
  for(int j = 0; j < 5; j++)
  {
    for(int i = 0; i < 10; i++)
    {
      ladrillo = RF_Engine::newTask<Ladrillo>(id);
      RF_Engine::getTask(ladrillo)->transform.position = {32 + 64*i, 10+ 20*j};
      RF_Engine::getTask(ladrillo)->graph = RF_AssetManager::Get<RF_Gfx2D>("ladrillos", colores[j]);
    }
  }

  preparePlayer();
}
void MainProcess::preparePlayer()
{
  gameStarted = false;
  if(plancha == "")
  {
    plancha = RF_Engine::newTask<Plancha>(id);
  }

  RF_Engine::getTask(plancha)->transform.position = {320, 460};

  if(pelota != "")
  {
    RF_Engine::sendSignal(pelota, S_KILL);
  }
  pelota = RF_Engine::newTask<Pelota>(id);
}

bool MainProcess::gameStarted = true;
