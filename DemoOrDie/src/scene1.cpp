#include "../include/scene1.h"
#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_primitive.h>

void Scene1::Start()
{
    RF_Engine::Debug(type);

    srand (RF_Engine::instance->Clock.currentTime);
    bgImg = RF_AssetManager::Get<RF_Gfx2D>("gfx", "manos");

    RF_Layer::Start();
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    for(i = 0; i < bgImg->w; i++)
    {
        for(j = 0; j < bgImg->h; j++)
        {
            RF_Primitive::putPixel(graph,i,j,RF_Primitive::getPixel(bgImg,i,j));
        }
    }

    nextX = colPos[rand()%5];

}
void Scene1::Update()
{
    if(8950 <= RF_Engine::instance->Clock.fixedCTime())
    {
        if(17100 > RF_Engine::instance->Clock.fixedCTime())
        {
            deltaCont+=RF_Engine::instance->Clock.deltaTime;
            if(0.5 <= deltaCont)
            {
                if(1.02 <= deltaCont)
                {
                    getBar(nextX,128);
                    deltaCont = 0.0;
                    nextX = colPos[rand()%5];
                }
                else
                {
                    setBar(nextX,128);
                }
            }
        }
        else if(34190 > RF_Engine::instance->Clock.fixedCTime())
        {
            if(-1 < nextX)
            {
                deltaCont = 1.02;
                getBar(nextX,128);
                nextX = -1;
            }

            deltaCont+=RF_Engine::instance->Clock.deltaTime;
            if(1.02 <= deltaCont)
            {
                revolveChanels();
                deltaCont = 0.0;
            }
        }
        else if(38385 > RF_Engine::instance->Clock.fixedCTime())
        {
            if(-1 == nextX){nextX = 0;}

            deltaCont+=RF_Engine::instance->Clock.deltaTime;
            if(0.75 <= deltaCont)
            {
              setBar(colPos[nextX],128, true);
              deltaCont = 0.0;
              nextX++;
            }
        }
        else
        {
          RF_Primitive::clearSurface(graph, 0x000000);
          RF_Engine::getTask<MainProcess>(father)->state() = 2;
          signal = RF_Structs::S_SLEEP;
        }
    }
    return;
}

void Scene1::setBar(int x, int width, bool black){
    for(i = x; i < x + width; i++)
    {
        for(j = 0; j < bgImg->h; j++)
        {
            if(0 <= i && 0 <= j && bgImg->w > i && bgImg->h > j)
            {
                tmpC = RF_Primitive::getPixel(bgImg,i,j);
                if(black) tmpC = 0xffffff;

                RF_Primitive::putPixel(graph,i,j,0xffffff - tmpC);
            }
        }
    }
}
void Scene1::getBar(int x, int width){
    for(i = x; i < x + width; i++)
    {
        for(j = 0; j < bgImg->h; j++)
        {
            if(0 <= i && 0 <= j && bgImg->w > i && bgImg->h > j)
            {
                RF_Primitive::putPixel(graph,i,j,RF_Primitive::getPixel(bgImg,i,j));
            }
        }
    }
}
void Scene1::revolveChanels(int mod){
    for(i = 0; i < graph->w; i++)
    {
        for(j = 0; j < graph->h; j++)
        {
            if(0 <= i && 0 <= j && graph->w > i && graph->h > j)
            {
                tmpC = RF_Primitive::getPixel(graph,i,j);
                SDL_GetRGB(tmpC,bgImg->format,&r,&g,&b);
                //int r_mod = (rand()%10) * mod;
                RF_Primitive::putPixel(graph,i,j,SDL_MapRGB(graph->format,255-g,b,r));
            }
        }
    }
}
