#ifndef SCENE1_H
#define SCENE1_H

#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_engine.h>

class Scene1 : public RF_Layer
{
    public:
        Scene1():RF_Layer("Scene1"){}
        virtual ~Scene1(){}

        virtual void Start();
        virtual void Update();

    private:
        SDL_Surface* bgImg;

        void setBar(int x, int width, bool black = false);
        void getBar(int x, int width);
        void revolveChanels(int mod = 0);

        float deltaCont = 0.5f;
        int colPos[5] = {0,128,256,384,512}, nextX;

        Uint32 tmpC;
        Uint8 r,g,b;
        int i,j;
};

#endif // SCENE1_H
