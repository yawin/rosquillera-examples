#ifndef RF_3D_H
#define RF_3D_H

#include "../include/rf_3d_object.h"
#include <RosquilleraReforged/rf_primitive.h>
#include <vector>
using namespace std;

enum RF_RenderModes{
    RM_Point,
    RM_Circles,
    RM_Mesh,
    RM_LandScape
};

class RF_3D
{
    public:
        RF_3D(){}
        virtual ~RF_3D(){}

        static void Draw(){}
        static void Draw_Only(SDL_Surface* screen, int objID = 0);
        static int loadObj(string file);

        static int& renderMode(){return _renderMode;}

        static vector<RF_3D_Object*> objectList;
        static int _renderMode;
};

#endif // RF_3D_H
