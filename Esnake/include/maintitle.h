#ifndef MAINTITLE_H
#define MAINTITLE_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_layer.h>
#include "../include/rotozoom.h"

class Title : public RF_Process
{
  public:
    Title():RF_Process("Title"){}
    virtual ~Title(){}

    virtual void Start();
};

class Maintitle : public RF_Layer
{
  public:
    Maintitle():RF_Layer("Maintitle"){}
    virtual ~Maintitle(){}

    virtual void Start();
    virtual void Update();

  private:
    bool pressed = false;

};

#endif //MAINTITLE_H
