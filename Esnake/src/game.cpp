#include "../include/game.h"
#include "../include/mainprocess.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_textmanager.h>

void Game::Start()
{
    RF_Layer::Start();
    RF_Primitive::clearSurface(graph, 0xF0F0F0);
    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    grid = new Rotozoom<int>(128,72);
    grid->setFactor(FACTOR);
    grid->setOffset(0.0,0.0);

    for(j = 0; j < grid->Height(); j++)
    {
      for(i = 0; i < grid->Width(); i++)
      {
        grid->addData(0);
      }
    }

    snake.position = {grid->Width() >> 1, grid->Height() >> 1};
    gameSpeed = 0.1;
    megaFoodTime = 10.0;
    megafood.x = -1;
    GetFood();
    ClearPutaditas();

    score = RF_TextManager::Write(to_string(0), {255,255,255}, scorePosition, id);
}

void Game::Update()
{
  if(snake.tam > 0)
  {
    KeyUpdate();
    MegaFoodUpdate();
    PutaditaUpdate();

    deltaCount += RF_Engine::instance->Clock.deltaTime;
    if(deltaCount >= gameSpeed)
    {
      deltaCount = 0.0;

      InputUpdate();
      SnakeUpdate();
      GridUpdate();
    }
  }
  else
  {
    DeadState();
  }
}

void Game::KeyUpdate()
{
  if(RF_Input::key[_up] || RF_Input::key[_w]){key = _up;}
  else if(RF_Input::key[_down] || RF_Input::key[_s]){key = _down;}
  else if(RF_Input::key[_left] || RF_Input::key[_a]){key = _left;}
  else if(RF_Input::key[_right] || RF_Input::key[_d]){key = _right;}
}

void Game::InputUpdate()
{
  if(key == _up && snake.dir != D_DOWN){snake.dir = D_UP;}
  else if(key == _down && snake.dir != D_UP){snake.dir = D_DOWN;}
  else if(key == _left && snake.dir != D_RIGHT){snake.dir = D_LEFT;}
  else if(key == _right && snake.dir != D_LEFT){snake.dir = D_RIGHT;}

  key = -1;
}
void Game::SnakeUpdate()
{
  switch(snake.dir)
  {
    case D_UP:
      snake.position.y--;
      break;
    case D_DOWN:
      snake.position.y++;
      break;
    case D_LEFT:
      snake.position.x--;
      break;
    case D_RIGHT:
      snake.position.x++;
      break;
  }

  if(snake.position.x < 0){snake.position.x = grid->Width()-1;}
  else if(snake.position.x >= grid->Width()){snake.position.x = 0;}

  if(snake.position.y < 0){snake.position.y = grid->Height()-1;}
  else if(snake.position.y >= grid->Height()){snake.position.y = 0;}

  tmp = grid->get(snake.position);

  if(tmp > 0)
  {
    RF_TextManager::Write("Has perdido", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, RF_Engine::MainWindow()->height()>>1}, id);
    RF_TextManager::Write("Pulsa ENTER", {255,255,255}, {RF_Engine::MainWindow()->width()>>1, 65 + RF_Engine::MainWindow()->height()>>1}, id);
    snake.tam = -1;
    return;
  }
  else if(tmp == -1)
  {
    Eaten();
  }
  else if(tmp == -2)
  {
    EatenMegaFood();
  }

  grid->set(snake.position, snake.tam);
}
void Game::GridUpdate()
{
  for(j = 0; j < grid->Height(); j++)
  {
    for(i = 0; i < grid->Width(); i++)
    {
      if(grid->get(i,j) > 0)
      {
        grid->set(i,j,grid->get(i,j)-1);
      }
    }
  }

  shaking.x = (float)grid->getOffset().x + shake.x * sin(RF_Engine::instance->Clock.currentTime*0.001);
  shaking.y = (float)grid->getOffset().y + shake.y * cos(RF_Engine::instance->Clock.currentTime*0.001);
  grid->setOffset((int)shaking.x, (int)shaking.y);
}

void Game::Eaten()
{
  snake.tam++;
  RF_TextManager::DeleteText(score);
  score = RF_TextManager::Write(to_string(snake.tam-3), {255,255,255}, scorePosition, id);

  shake.x += (float)(1+rand()%5)/5;
  shake.y += (float)(1+rand()%5)/5;

  GetFood();
}

void Game::MegaFoodUpdate()
{
  if(megaFoodTime > 0.0)
  {
    megaFoodTime -= RF_Engine::instance->Clock.deltaTime;
  }
  else if(megafood.x == -1)
  {
    GetMegaFood();
  }
  else
  {
    EatenMegaFood(false);
  }
}

void Game::EatenMegaFood(bool eaten)
{
  if(eaten) shake = shake*0.5;
  else GetPutadita();

  megaFoodTime = 30 + rand()%30;
  grid->set(megafood, 0);
  megafood.x = -1;
}

void Game::GetMegaFood()
{
  do
  {
    tmp = snake.rango - rand()%(snake.rango*4);
    i = snake.position.x + tmp;
    if(i < 0){ i = grid->Width() + i; }
    else if(i >= grid->Width())
    {
      i = i - grid->Width();
    }

    tmp = snake.rango - rand()%(snake.rango*4);
    j = snake.position.y + tmp;
    if(j < 0){ j = grid->Height() + j; }
    else if(j >= grid->Height())
    {
      j = j - grid->Height();
    }

  }while(grid->get(i,j) != 0);

  grid->set(i,j,-2);
  megaFoodTime = 10.0;
  megafood.x = i; megafood.y = j;
}

void Game::Draw()
{
  for(j = 0; j < graph->h; j++)
  {
    for(i = 0; i < graph->w; i++)
    {
      tmp = grid->getRZ(i,j);
      color = BACKCOLOR;
      if(tmp > 0)
      {
        color = BLUE;
      }
      else if(tmp == -1)
      {
        color = RED;
      }
      else if(tmp == -2)
      {
        color = GREEN;
      }

      RF_Primitive::putPixel(graph, i, j, color);
    }
  }
}

void Game::GetFood()
{
  if(snake.tam%10 == 0){ snake.rango += 5; }

  do
  {
    tmp = snake.rango - rand()%(snake.rango*2);
    i = snake.position.x + tmp;
    if(i < 0){ i = grid->Width() + i; }
    else if(i >= grid->Width())
    {
      i = i - grid->Width();
    }

    tmp = snake.rango - rand()%(snake.rango*2);
    j = snake.position.y + tmp;
    if(j < 0){ j = grid->Height() + j; }
    else if(j >= grid->Height())
    {
      j = j - grid->Height();
    }

  }while(grid->get(i,j) != 0);

  grid->set(i,j,-1);
}

void Game::DeadState()
{
  if(!pressed && RF_Input::key[_return])
  {
    pressed = true;
  }
  else if(pressed && !RF_Input::key[_return])
  {
    RF_Primitive::clearSurface(graph, 0x000000);
    RF_Engine::getTask<MainProcess>(father)->state() = 1;
    signal = RF_Structs::S_SLEEP;
  }
}

void Game::PutaditaUpdate()
{
  if(putaditaTime > 0.0)
  {
    putaditaTime -= RF_Engine::instance->Clock.deltaTime;
  }
  else if(putadita != P_FOO)
  {
    grid->setFactor(FACTOR);
    grid->setRotation(0.0);
    putadita = P_FOO;
  }
}
void Game::ClearPutaditas()
{
  for(i = 0; i < P_FOO; i++)
  {
    putaditas[i] = false;
  }
}
void Game::GetPutadita()
{
  if(putadita != P_FOO) return;

  do
  {
    putadita = rand()%P_FOO;
  }while(putaditas[!putadita]);

  putCont++;
  if(putCont >= P_FOO){ ClearPutaditas(); }
  else{ putaditas[putadita] = true; }

  putaditaTime = 20.0;

  switch(putadita)
  {
    case P_ZOOM:
      grid->setFactor(0.3);
      break;
    case P_CONTROLS:
      grid->setFactor(-FACTOR);
      break;
    case P_ROTO:
      grid->setOffset(0.0,0.0);
      grid->setFactor(0.2);
      grid->setRotation(45);
      break;
  }
}
