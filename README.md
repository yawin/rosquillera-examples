# Rosquillera Framework Examples

Proyectos de ejemplo de uso de La Rosquillera Framework - Reforged

## Introducción
**La Rosquillera Framework** es un framework para hacer videojuegos con SDL2 que tiene como objetivo que el usuario pueda hacer juegos como rosquillas (o sea muchos, muy rápido y sin romperse la cabeza).

En este repositorio se compilan una serie de proyectos a modo ejemplo que sirvan de ayuda para entender el uso de este framework y visualizar el potencial del mismo.

### ¿La Rosquillera Framework?
Sí, puedes encontrar toda la información que necesites al respecto en la siguiente dirección: http://rosquillera.yawin.es