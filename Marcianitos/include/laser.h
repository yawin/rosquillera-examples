#ifndef SHOOT_H
#define SHOOT_H

#include <RosquilleraReforged/rf_process.h>

class Laser : public RF_Process
{
  public:
    Laser():RF_Process("Laser"){}
    virtual ~Laser(){}

    virtual void Start();
    virtual void Update();
    static void Shoot(RF_Structs::Vector2<float> origin, RF_Structs::Vector2<float> speed, string father = "");

  private:
    RF_Structs::Vector2<float> speed;
    RF_Structs::Vector2<float> tmp_position;
    RF_Process* f;
};

#endif //SHOOT_H
