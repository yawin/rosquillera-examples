#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <RosquilleraReforged/rf_layer.h>
#include <SDL2/SDL.h>

class Background : public RF_Layer
{
  public:
    Background():RF_Layer("Background"){}
    virtual ~Background(){}

    virtual void Start();
    virtual void Update();
    virtual void Draw();

  private:
    SDL_Surface *fondo;
    float offset = 0.0;
    int ioffset = 0, tmp_j;
};

#endif //BACKGROUND_H
