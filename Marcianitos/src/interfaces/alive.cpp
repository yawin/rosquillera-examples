#include "../include/interfaces/alive.h"
#include <RosquilleraReforged/rf_assetmanager.h>

SDL_Surface* Alive::explosion()
{
  expl += RF_Engine::instance->Clock.deltaTime*16.0;
  if(expl < 16.9)
  {
    RF_AssetManager::loadAssetPackage("./res/explosion");
    return RF_AssetManager::Get<RF_Gfx2D>("explosion", "exp" + to_string((int)expl));
  }

  return nullptr;
}
