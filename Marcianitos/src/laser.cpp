#include "../include/laser.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <RosquilleraReforged/rf_collision.h>

#include "../include/enemy.h"
#include "../include/interfaces/alive.h"

void Laser::Shoot(RF_Structs::Vector2<float> origin, RF_Structs::Vector2<float> speed, string father)
{
  Laser* laser = RF_Engine::getTask<Laser>(RF_Engine::newTask<Laser>(father));
  laser->transform.position = origin;
  laser->speed = speed;
}

void Laser::Start()
{
  zLayer = 2;
  RF_AssetManager::loadAssetPackage("./res/common");
  graph = RF_AssetManager::Get<RF_Gfx2D>("common", "laser");
  f = RF_Engine::getTask(father);
}

void Laser::Update()
{
  transform.position.x += speed.x * RF_Engine::instance->Clock.deltaTime;
  transform.position.y += speed.y * RF_Engine::instance->Clock.deltaTime;

  if( transform.position.x + graph->w < 0   ||
      transform.position.x - graph->w > 640 ||
      transform.position.y + graph->h < 0   ||
      transform.position.y - graph->h > 480
  )
  {
    signal = RF_Structs::S_KILL;
  }

  if(f->type == "Nave")
  {
    Enemy *collided = reinterpret_cast<Enemy*>(RF_Collision::checkCollisionByType(this, "Enemy"));
    if(collided)
    {
      collided->doDamage(1);
      signal = RF_Structs::S_KILL;
    }
  }
}
