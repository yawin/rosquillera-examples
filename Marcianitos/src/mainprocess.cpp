#include "../include/mainprocess.h"
#include <RosquilleraReforged/rf_engine.h>

#include "../include/background.h"
#include "../include/nave.h"
#include "../include/enemy.h"

void MainProcess::Start()
{
    RF_Engine::addWindow("Marcianitos", 640, 480);
    RF_Engine::newTask<Background>(id);
    RF_Engine::newTask<Nave>(id);
}

void MainProcess::Update()
{
  if(RF_Input::key[RF_Structs::_esc])
  {
    RF_Engine::Status() = false;
  }

  if(rand()%100000 < 300)
  {
    RF_Engine::newTask<BasicEnemy>(id);
  }
}
