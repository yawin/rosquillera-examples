#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_collision.h>

#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;
using namespace RF_Structs;

class Verde : public RF_Process
{
  public:
    Verde():RF_Process("Verde"){}
    virtual ~Verde(){}

    virtual void Start()
    {
      padre = RF_Engine::getTask(father);
      zLayer = padre->zLayer-1;
      p_dims = old_p_dims = padre->getDimensions();
      graph = SDL_CreateRGBSurface(0, p_dims.w, p_dims.h, 32, 0, 0, 0, 0);
    }

    virtual void Draw()
    {
      transform.position = padre->transform.position;
      p_dims = padre->getDimensions();

      if(p_dims.w != old_p_dims.w || p_dims.h != p_dims.h)
      {
        old_p_dims = p_dims;
        if(graph != nullptr)
        {
          SDL_FreeSurface(graph);
        }

        graph = SDL_CreateRGBSurface(0, p_dims.w, p_dims.h, 32, 0, 0, 0, 0);
      }

      RF_Primitive::clearSurface(graph, 0x00FF00);
    }

  private:
    RF_Process *padre;
    SDL_Rect p_dims, old_p_dims;
};

class Azul : public RF_Process
{
  public:
    Azul():RF_Process("Azul"){}
    virtual ~Azul(){}

    virtual void Start()
    {
      transform.position = {240.0,240.0};
      transform.position.x -= 50;

      graph = RF_AssetManager::Get<RF_Gfx2D>("img", "azul");
      RF_Engine::newTask<Verde>(id);
    }

    virtual void Update()
    {
      if(RF_Input::key[_q]){ transform.rotation -= 100.0 * RF_Engine::instance->Clock.deltaTime; }
      else if(RF_Input::key[_e]){ transform.rotation += 100.0 * RF_Engine::instance->Clock.deltaTime; }

      if(RF_Input::key[_r]){ transform.scale.x = transform.scale.y = transform.scale.x + RF_Engine::instance->Clock.deltaTime; }
      else if(RF_Input::key[_f]){ transform.scale.x = transform.scale.y = transform.scale.x - RF_Engine::instance->Clock.deltaTime; }

      if(RF_Input::key[_a] && !RF_Input::key[_d]){ transform.position.x -= 40.0 * RF_Engine::instance->Clock.deltaTime; }
      else if(RF_Input::key[_d] && !RF_Input::key[_a]){ transform.position.x += 40.0 * RF_Engine::instance->Clock.deltaTime; }
      else if(RF_Input::key[_w] && !RF_Input::key[_s]){ transform.position.y -= 40.0 * RF_Engine::instance->Clock.deltaTime;}
      else if(RF_Input::key[_s] && !RF_Input::key[_w]){ transform.position.y += 40.0 * RF_Engine::instance->Clock.deltaTime; }

      if(RF_Collision::checkCollisionByType(this, "Rojo", RF_CT_PP) != nullptr)
      {
        RF_Engine::Debug("Colisiono");
      }
    }
};

class Rojo : public RF_Process
{
  public:
    Rojo():RF_Process("Rojo"){}
    virtual ~Rojo(){}
    virtual void Start()
    {
      transform.position.x = 315;
      transform.position.y = 240;
      graph = RF_AssetManager::Get<RF_Gfx2D>("img", "rojo");
    }
};

class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    virtual void Start()
    {
      RF_Engine::addWindow("Colisiones", 480, 480);
      //RF_Engine::MainWindow()->setBackColor(200,200,200);

      RF_AssetManager::loadAssetPackage("img");

      RF_Engine::newTask<Rojo>(id);
      RF_Engine::newTask<Azul>(id);
    }

    virtual void Update()
    {
      if(RF_Input::key[_esc] || RF_Input::key[_close_window])
      {
        RF_Engine::Status()=false;
      }
    }
};

int main(int argc, char *argv[])
{
	if(argc > 0)
	{
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());
	}

	RF_Engine::Start<MainProcess>(true);
	return 0;
}


#ifdef WIN
	int __stdcall WinMain(void*, void*, char*, int)
	{
		main(0, NULL);
		return 0;
	}
#endif
