#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_debugconsole.h>

#include "mainprocess.h"

#include <unistd.h>
#include <iostream>
#include <string>
using namespace std;

void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
	for(RF_Process *p : RF_TaskManager::instance->getTaskList())
	{
		if(p->type != "RF_Text")
		{
			RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
		}
	}
}

int main(int argc, char *argv[])
{
	if(argc > 0)
	{
		string arg(argv[0]);
	  arg.erase(arg.find_last_of('/'), arg.size());
		chdir(arg.c_str());
	}

	RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
	RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));
	RF_Engine::Start<MainProcess>(true);
	return 0;
}


#ifdef WIN
	int __stdcall WinMain(void*, void*, char*, int)
	{
		main(0, NULL);
		return 0;
	}
#endif
